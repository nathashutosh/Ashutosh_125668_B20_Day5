<?php

$a= "10";
$b= 10;

//$a === $b	Identical	TRUE if $a is equal to $b, and they are of the same type..

if($a===$b){
    echo "$a is equal to $b ";
}
else {
    echo "$a is equal not $b ";
}

echo "<hr>";

$c= "50";
$d= 50;

//$a !== $b	Not identical	TRUE if $a is not equal to $b, or they are not of the same type.

if($c!==$d){
    echo "$c is equal not $d ";
}
else {
    echo "$d is equal $d ";
}

?>