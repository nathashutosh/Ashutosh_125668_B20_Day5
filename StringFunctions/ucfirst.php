<?php
$foo = 'hello world!';
$foo = ucfirst($foo);             // Hello world!

$bar = 'HELLO WORLD!';
$bar1 = ucfirst($bar);             // HELLO WORLD!
$bar2 = ucfirst(strtolower($bar1)); // Hello world!

echo "$bar1";
echo "<hr>";
echo "$bar2";
echo "<hr>";


?>