<?php
$foo = 'hello world!';
$foo = ucwords($foo);             // Hello world!

$bar = 'HELLO WORLD!';
$bar1 = ucwords($bar);             // HELLO WORLD!
$bar2 = ucwords(strtolower($bar1)); // Hello world!

echo "$bar1";
echo "<hr>";
echo "$bar2";
echo "<hr>";
