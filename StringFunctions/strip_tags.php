<?php
$text = '<p>Test paragraph.</p><!-- Comment --> <a href="#fragment">Other text</a>';
echo strip_tags($text);
echo "\n";

// Allow <p> and <a>
echo strip_tags($text, '<p><a>');

echo "<hr>";
$var = '<p>Wellcome to BITM.</p><!-- Comment --> <a href="#">PHP Devloper</a>';

echo strip_tags($var);
echo "\n";

// Allow <p> and <a>
echo strip_tags($var, '<a>');




?>